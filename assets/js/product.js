var buttonView = document.querySelectorAll(".view-change button");
var productList = document.querySelectorAll(".product-list>ul");
for (var i = 0; i < buttonView.length; i++) {
  buttonView[i].addEventListener("click", function () {
    console.log(this.getAttribute("data-list"));
    for (var j = 0; j < productList.length; j++) {
      productList[j].classList.remove("active");
    }
    for (var j = 0; j < buttonView.length; j++) {
      buttonView[j].classList.remove("active");
    }
    this.classList.add("active");
    document
      .getElementById(this.getAttribute("data-list"))
      .classList.add("active");
  });
}
document.getElementById("button-filter").addEventListener("click", function () {
  this.nextElementSibling.classList.toggle("active");
});
